node {
    def scmVars = checkout([
               $class : 'GitSCM',
               branches : [[name: '*/master']],
               doGenerateSubmoduleConfigurations: false,
               extensions : [/*[$class: 'CleanBeforeCheckout']*/],
               submoduleCfg : [],
               userRemoteConfigs: [[credentialsId: 'local-bb', url: 'https://bitbucket.org/Yashrajp9/live-project.git']]
           ])
   stage('test') {
       println 'some text'
       println scmVars.GIT_BRANCH
   }
}


node{
   stage('SCM Checkout'){
     git 'https://bitbucket.org/Yashrajp9/live-project'
   }
   stage('ansible-playbook'){
      ansiblePlaybook credentialsId: 'b5e29134-9482-49d0-ae3d-8b4e2f0e71f3',installation: 'Ansible2', inventory: '${workspace}/hosts', limit: '172.31.26.240', playbook: '${workspace}/docker.yml'
   }
}